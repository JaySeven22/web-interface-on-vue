import HomePage from "../../components/HomePage.vue"
import AuthorizationPage from "../../components/AuthorizationPage.vue"
import { createWebHistory, createRouter } from "vue-router"
import RegisterPage from "../RegisterPage.vue"
import ListAllUsers from "../../components/ListAllUsers.vue"


// Создаю роутинг

const routes = [
    { path: '/homePage', component: HomePage },
    { path: '/authorizationPage', component: AuthorizationPage},
    { path: '/', component: RegisterPage},
    { path: '/listAllUsers', component: ListAllUsers},
    { path: '/:pathMatch(.*)*', redirect: "/authorizationPage"}  // Редирект при использовании не существующего роута
  ]

  const routing = createRouter({
   
    history: createWebHistory(),
    routes, 
  })
  
 
  export default routing ;


 

  
